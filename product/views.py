from django.shortcuts import render_to_response, get_object_or_404, Http404, render
from django_easyfilters import FilterSet
from django.template import RequestContext
from product.models import Category, Product 
from django.views.generic import DetailView, ListView
from django.core import serializers
# import json
from django.http import HttpResponse

def index(request):
	context = RequestContext(request)
	category = Category.objects.all()
	context_dict = {'category':category}
	return render_to_response('home/index.html', context_dict, context)

def about(request):
	context = RequestContext(request)
	category = Category.objects.all()
	context_dict = {'category':category}
	return render_to_response('home/about.html', context_dict, context)

def privacy(request):
	context = RequestContext(request)
	category = Category.objects.all()
	context_dict = {'category':category}
	return render_to_response('home/privacy-policy.html', context_dict, context)


# import django_filters

class ProductFilter(FilterSet):
	fields = ['flipkart_price', 'brand', 'os', 'processor']


def category(request, category_name_url):
	# context_dict = {'category_name': category_name_url}
	try:
		category = Category.objects.get(name=category_name_url)
		products = Product.objects.filter(category=category)
		productfilter = ProductFilter(products, request.GET)
	except Category.DoesNotExist:
		pass
	return render(request, "product/product.html", {'products': productfilter.qs,
                                             'productfilter': productfilter, 'category_name': category_name_url})


"""
def category(request, category_name_url):
	context_dict = {'category_name':category_name_url}
	try:
		category = Category.objects.get(name=category_name_url)
		products = Product.objects.filter(category=category)
		productfilter = ProductFilter(request.GET, products)
		context_dict['products'] = productfilter.qs
		# context_dict['category'] = category
		context_dict['productfilter'] = productfilter
		# json_products = serializers.serialize("json", productfilter)
	except Category.DoesNotExist:
		pass
	
	# template_context = {'my_json': json.dumps(json_products)}
	return render(request, 'product/product.html', context_dict)
"""

def detail(request, slug, category_url):
	try:
		category = Category.objects.get(name=category_url)
		product = Product.objects.get(slug=slug)
	
	except Product.DoesNotExist:
		raise Http404

	return render_to_response('product/product-detail.html', {'category_name': category_url, 'product': product})









