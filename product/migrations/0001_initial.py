# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=128)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('slug', models.SlugField(null=True, blank=True)),
                ('brand', models.CharField(max_length=140)),
                ('processor', models.CharField(max_length=140)),
                ('screensize', models.CharField(max_length=140)),
                ('ram', models.CharField(max_length=140)),
                ('os', models.CharField(max_length=140)),
                ('memory', models.CharField(max_length=140)),
                ('imgurl', models.CharField(max_length=500)),
                ('flipkart_price', models.IntegerField(default=True)),
                ('snapdeal_price', models.IntegerField(default=True, blank=True)),
                ('amazon_price', models.IntegerField(default=True, blank=True)),
                ('description', models.TextField(default=True, blank=True)),
                ('category', models.ForeignKey(to='product.Category')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
