# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='amazon_url',
            field=models.CharField(default=True, max_length=b'200', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='flipkart_url',
            field=models.IntegerField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='snapdeal_url',
            field=models.CharField(default=True, max_length=b'200', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='amazon_price',
            field=models.IntegerField(default=True, blank=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(default=True, max_length=b'200', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='flipkart_price',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='snapdeal_price',
            field=models.IntegerField(default=True, blank=0),
            preserve_default=True,
        ),
    ]
