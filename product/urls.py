from django.conf.urls import patterns, include, url
from django.contrib import admin
from product.views import index, about, privacy, category, detail

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^about/$', about, name='index'),
    url(r'^privacy/$', privacy, name='index'),
    url(r'^(?P<category_name_url>\w+)/$', category, name='category'),
    url(r'^(?P<category_url>\w+)/(?P<slug>[-\w]+)$', detail , name='detail'),
)
