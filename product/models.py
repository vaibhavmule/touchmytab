
from django.db import models
from django.utils.text import slugify

class Category(models.Model):
	name = models.CharField(max_length=128, unique=True)

	def __unicode__(self):
		return self.name

		
class Product(models.Model):
	title = models.CharField(max_length=140)
	slug = models.SlugField(null = True, blank = True)
	brand = models.CharField(max_length=140)
	processor = models.CharField(max_length=140)
	screensize = models.CharField(max_length=140)
	ram = models.CharField(max_length=140)
	os = models.CharField(max_length=140)
	memory = models.CharField(max_length=140)
	imgurl = models.CharField(max_length=500)
	flipkart_price = models.IntegerField(default=0)
	snapdeal_price = models.IntegerField(default=True, blank=0)
	amazon_price = models.IntegerField(default=True, blank=0)
	flipkart_url = models.CharField(default=True, blank=True, max_length="200")
	snapdeal_url = models.CharField(default=True, blank=True, max_length="200")
	amazon_url = models.CharField(default=True, blank=True, max_length="200")
	description = models.TextField(default=True, blank=True)
	category = models.ForeignKey(Category)

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.title)
		super(Product, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.title









